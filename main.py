import math
import random
from enum import Enum
from typing import Dict

import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "The Space Survivor"

PERK_SPAWN_INTERVAL_SEC = 10
STARSHIP_SCALE = 0.5
ASTEROID_COUNT = 5
HIT_SCORE_POINTS = 20

TEXT_START_X = 10
TEXT_START_Y = 10
TEXT_FONT_SIZE = 13

MUSIC_VOLUME = 0.5


class StarshipMovement(Enum):
    UP: str = "up"
    DOWN: str = "down"
    LEFT: str = "left"
    RIGHT: str = "right"


class GameState(Enum):
    GAME_PLAYING: str = "game_playing"
    GAME_PLAYER_RESPAWNING: str = "player_respawning"
    GAME_OVER: str = "game_over"


class LoopActionInFrameMixin:
    def loop_actor_in_frame(self):
        if self.left > SCREEN_WIDTH:
            self.right = 0
        if self.right < 0:
            self.left = SCREEN_WIDTH
        if self.top > SCREEN_HEIGHT:
            self.bottom = 0
        if self.bottom < 0:
            self.top = SCREEN_HEIGHT


class BaseSprite(arcade.Sprite):
    def __init__(self, *args, angle=0, speed=0, x_direction=0, y_direction=0, **kwargs):
        super().__init__(*args, angle=angle, **kwargs)
        self.speed = speed
        self.x_direction = x_direction
        self.y_direction = y_direction

    def update_position(self):
        self.center_x += self.speed * self.x_direction
        self.center_y += self.speed * self.y_direction


class StarshipSprite(BaseSprite, LoopActionInFrameMixin):
    class BulletSprite(BaseSprite):
        SPEED = 6

        def __init__(self, *args, **kwargs):
            super().__init__(
                filename=":resources:images/space_shooter/laserRed01.png",
                *args,
                speed=self.SPEED,
                scale=0.5,
                **kwargs
            )

        def update(self):
            super().update_position()

    MAX_SPEED = 3

    def __init__(self):
        super().__init__(
            filename=":resources:images/space_shooter/playerShip1_orange.png",
            center_x=SCREEN_WIDTH // 2,
            center_y=SCREEN_HEIGHT // 2,
            scale=STARSHIP_SCALE,
        )

        self.movement_control: Dict[StarshipMovement, bool] = {
            StarshipMovement.UP: False,
            StarshipMovement.DOWN: False,
            StarshipMovement.LEFT: False,
            StarshipMovement.RIGHT: False,
        }

        self.bullets = arcade.SpriteList()
        self.lives = 1

    def update(self):
        self.move()
        self.calculate_direction()
        super().update_position()
        self.slow_down()
        self.loop_actor_in_frame()
        self.remove_bullets()

    def move(self):
        if self.movement_control[StarshipMovement.LEFT]:
            self.angle += 5
        if self.movement_control[StarshipMovement.RIGHT]:
            self.angle -= 5
        if self.movement_control[StarshipMovement.UP]:
            self.speed_up()

    def calculate_direction(self):
        self.x_direction = math.sin(math.radians(-self.angle))
        self.y_direction = math.cos(math.radians(-self.angle))

    def slow_down(self):
        if self.speed > 0:
            self.speed -= 0.2

    def speed_up(self):
        if self.speed < self.MAX_SPEED:
            self.speed += 1

    def fire(self):
        bullet = self.BulletSprite(
            center_x=self.center_x,
            center_y=self.center_y,
            angle=self.angle,
            x_direction=self.x_direction,
            y_direction=self.y_direction
        )
        self.bullets.append(bullet)

    def remove_bullets(self):
        for bullet in self.bullets:
            if (
                    0 > bullet.center_x
                    or bullet.center_x > SCREEN_WIDTH
                    or 0 > bullet.center_y
                    or bullet.center_y > SCREEN_HEIGHT
            ):
                self.bullets.remove(bullet)
                print(f"Removed bullet {bullet.center_x}, {bullet.center_y}")

    def respawn(self):
        self.center_x = SCREEN_WIDTH // 2
        self.center_y = SCREEN_HEIGHT // 2
        self.angle = 0
        self.alpha = 0


class AsteroidSprite(BaseSprite, LoopActionInFrameMixin):
    def __init__(self, speed):
        super().__init__(filename=":resources:images/space_shooter/meteorGrey_big3.png", speed=speed)
        random_spawn(self)

    def update(self):
        super().update_position()
        self.loop_actor_in_frame()


class ExtraLivePerk(BaseSprite, LoopActionInFrameMixin):
    def __init__(self):
        super().__init__(
            filename=":resources:images/space_shooter/playerLife1_green.png", speed=1
        )
        random_spawn(self)

    def update(self):
        super().update_position()
        self.loop_actor_in_frame()

    @staticmethod
    def apply(starship: StarshipSprite):
        print("Added on live to our Starship")
        starship.lives += 1


def random_spawn(sprite: BaseSprite):
    if random.choice([True, False]):
        sprite.center_x = random.choice([0, SCREEN_WIDTH])
        sprite.center_y = random.randint(0, SCREEN_HEIGHT)
    else:
        sprite.center_x = random.randint(0, SCREEN_HEIGHT)
        sprite.center_y = random.choice([0, SCREEN_WIDTH])

    sprite.x_direction = random.uniform(-1, 1)
    sprite.y_direction = random.uniform(-1, 1)


class MyGame(arcade.Window):
    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
        self.game_state = GameState.GAME_PLAYING
        self.respawning_update_tick = 0
        self.score = 0
        self.background = None
        self.starship = None
        self.asteroids = None
        self.perk = None
        self.lives_element_hud = None
        self.score_element_hud = None

        self.laser_sound = None
        self.hit_sound = None
        self.perk_sound = None

        self.music = None

    def spawn_perk(self, delta_time: float = 0):
        if self.perk:
            self.perk.kill()

        self.perk = ExtraLivePerk()
        print(f"Spawned perk at {self.perk.center_x}, {self.perk.center_x}")

    def spawn_asteroid(self):
        asteroid_sprite = AsteroidSprite(2)
        self.asteroids.append(asteroid_sprite)
        print(f"Spawned asteroid at {asteroid_sprite.center_x}, {asteroid_sprite.center_y}")

    def setup(self):
        self.background = arcade.load_texture(":resources:images/backgrounds/stars.png")
        self.starship = StarshipSprite()
        self.asteroids = arcade.SpriteList()
        self.perk = ExtraLivePerk()

        for i in range(ASTEROID_COUNT):
            self.spawn_asteroid()

        arcade.schedule(self.spawn_perk, interval=PERK_SPAWN_INTERVAL_SEC)

        self.lives_element_hud = arcade.Text(f"Starship lives: {self.starship.lives}", start_x=TEXT_START_X,
                                             start_y=TEXT_START_Y, font_size=TEXT_FONT_SIZE)

        self.score_element_hud = arcade.Text(f"Score: {self.score}", start_x=SCREEN_WIDTH - 200,
                                             start_y=TEXT_START_Y, font_size=TEXT_FONT_SIZE * 1.5)

        self.laser_sound = arcade.load_sound(":resources:sounds/hurt5.wav")
        self.hit_sound = arcade.load_sound(":resources:sounds/explosion1.wav")
        self.perk_sound = arcade.load_sound(":resources:sounds/coin3.wav")

    def on_update(self, delta_time: float):
        self.lives_element_hud.text = f"Starship lives: {self.starship.lives}"
        self.score_element_hud.text = f"Score: {self.score}"

        if self.game_state == GameState.GAME_PLAYING:
            self.starship.update()
            self.starship.bullets.update()
            self.asteroids.update()

            if self.perk:
                self.perk.update()
                if arcade.check_for_collision(self.starship, self.perk):
                    self.perk_sound.play()
                    self.perk.apply(self.starship)
                    self.perk = None

            if starship_colliding_asteroids := arcade.check_for_collision_with_list(self.starship, self.asteroids):
                print(f"Starship collided with asteroids: {starship_colliding_asteroids}")
                for asteroid in starship_colliding_asteroids:
                    self.hit_sound.play()
                    if self.starship.lives > 1:
                        self.starship.lives -= 1
                        print(f"Starship lost live. Remaining lives: {self.starship.lives}")
                        self.starship.respawn()
                        self.game_state = GameState.GAME_PLAYER_RESPAWNING
                    else:
                        self.game_state = GameState.GAME_OVER

                    self.asteroids.remove(asteroid)
                    self.spawn_asteroid()

            for asteroid in self.asteroids:
                if asteroid_colliding_bullets := arcade.check_for_collision_with_list(asteroid, self.starship.bullets):
                    print(f"Asteroid {asteroid} collided with bullets: {asteroid_colliding_bullets}")
                    self.score += HIT_SCORE_POINTS
                    if asteroid_colliding_bullets:
                        self.hit_sound.play()
                        self.asteroids.remove(asteroid)
                        self.spawn_asteroid()

                    for bullet in asteroid_colliding_bullets:
                        self.starship.bullets.remove(bullet)
        elif self.game_state == GameState.GAME_PLAYER_RESPAWNING:
            self.starship.update()
            self.starship.bullets.update()
            self.asteroids.update()

            if self.perk:
                self.perk.kill()
                self.perk = None

            self.respawning_update_tick += 1
            self.starship.alpha = self.respawning_update_tick

            if self.respawning_update_tick > 180:
                self.respawning_update_tick = 0
                self.starship.alpha = 255
                self.game_state = GameState.GAME_PLAYING

        elif self.game_state == GameState.GAME_OVER:
            print("Game over")

    def on_draw(self):
        self.clear()
        if self.game_state == GameState.GAME_PLAYING or self.game_state == GameState.GAME_PLAYER_RESPAWNING:
            arcade.draw_texture_rectangle(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2, SCREEN_WIDTH, SCREEN_HEIGHT,
                                          self.background)
            self.lives_element_hud.draw()
            self.starship.draw()
            self.starship.bullets.draw()
            self.asteroids.draw()
            if self.perk:
                self.perk.draw()

            self.score_element_hud.draw()

        elif self.game_state == GameState.GAME_OVER:
            arcade.draw_text("Game Over", start_x=SCREEN_WIDTH // 2, start_y=SCREEN_HEIGHT // 2,
                             font_size=TEXT_FONT_SIZE * 2, width=200, align="center", anchor_x="center",
                             anchor_y="center")

    def on_key_press(self, key, modifiers: int):
        if key == arcade.key.LEFT:
            self.starship.movement_control[StarshipMovement.LEFT] = True
        if key == arcade.key.RIGHT:
            self.starship.movement_control[StarshipMovement.RIGHT] = True
        if key == arcade.key.UP:
            self.starship.movement_control[StarshipMovement.UP] = True
        if key == arcade.key.DOWN:
            self.starship.movement_control[StarshipMovement.DOWN] = True
        if key == arcade.key.SPACE:
            arcade.play_sound(self.laser_sound, speed=random.random() * 3 + 0.5)
            self.starship.fire()

    def on_key_release(self, key, modifiers: int):
        if key == arcade.key.LEFT:
            self.starship.movement_control[StarshipMovement.LEFT] = False
        if key == arcade.key.RIGHT:
            self.starship.movement_control[StarshipMovement.RIGHT] = False
        if key == arcade.key.UP:
            self.starship.movement_control[StarshipMovement.UP] = False
        if key == arcade.key.DOWN:
            self.starship.movement_control[StarshipMovement.DOWN] = False


if __name__ == "__main__":
    window = MyGame()
    window.setup()
    arcade.run()

